use std::{time::{Duration, Instant}, sync::Arc};

use rspotify::{prelude::*, Credentials, scopes, OAuth, AuthCodeSpotify, Config, model::TrackId};
use serde::Serialize;
use tokio::sync::{RwLock, Mutex};
use warp::Filter;

#[macro_use]
extern crate log;

#[derive(Debug, Serialize)]
struct NowPlayingInfo {
    state: State,
    title: String,
    track_id: Option<TrackId>,
    album: String,
    album_artwork: Option<String>,
    artist: String,
    artist_artwork: Option<String>,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "snake_case")]
enum State {
    Playing,
    Recent,
}

#[derive(Clone)]
struct SpotifyHandler(Arc<HandlerState>);

struct HandlerState {
    client: RwLock<AuthCodeSpotify>,
    status: RwLock<NowPlayingInfo>,
    last_update: Mutex<Instant>,
}

impl HandlerState {
    async fn update_now_playing(&self) -> anyhow::Result<()> {
        const MIN_UPDATE_INTERVAL: Duration = Duration::from_millis(5000);

        let now = Instant::now();
        if now - *self.last_update.lock().await > MIN_UPDATE_INTERVAL {
            let now_playing = self.get_now_playing().await?;
            if let Some(now_playing) = now_playing {
                *self.status.write().await = now_playing;
            } else {
                self.status.write().await.state = State::Recent;
            }
            *self.last_update.lock().await = now;
        }
        Ok(())
    }

    async fn get_now_playing(&self) -> anyhow::Result<Option<NowPlayingInfo>> {
        let client = self.client.read().await;
        let playing = client.current_playing(None, None::<Vec<_>>).await?;
        if let Some(playing) = playing {
            if let Some(item) = playing.item {
                match item {
                    rspotify::model::PlayableItem::Track(track) => {
                        if track.artists.is_empty() {
                            return Ok(None);
                        }
                        let artist = client.artist(&track.artists.get(0).unwrap().id.clone().unwrap()).await?;
                        Ok(Some(NowPlayingInfo {
                            state: if playing.is_playing {
                                State::Playing
                            } else {
                                State::Recent
                            },
                            title: track.name,
                            track_id: track.id,
                            album: track.album.name,
                            album_artwork: track.album.images.get(0).map(|i| i.url.clone()),
                            artist: artist.name,
                            artist_artwork: artist.images.get(0).map(|i| i.url.clone()),
                        }))
                    },
                    _ => Ok(None),
                }
            } else {
                Ok(None)
            }
        } else {
            Ok(None)
        }
    }
}

impl SpotifyHandler {
    pub fn new(client: AuthCodeSpotify) -> Self {
        let state = Arc::new(HandlerState {
            client: RwLock::new(client),
            status: RwLock::new(NowPlayingInfo {
                state: State::Recent,
                title: "Beep".to_string(),
                track_id: None,
                album: "Boop".to_string(),
                artist: "The server".to_string(),
                album_artwork: None,
                artist_artwork: None,
            }),
            last_update: Mutex::new(Instant::now()),
        });

        Self(state)
    }
}

async fn get_status(handler: SpotifyHandler) -> Result<Box<dyn warp::Reply>, warp::Rejection> {
    if let Err(e) = handler.0.update_now_playing().await {
        error!("Failed to update now playing: {}", e);
        Err(warp::reject())
    } else {
        Ok(Box::new(warp::reply::json(&*handler.0.status.read().await)))
    }
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    env_logger::init();
    let creds = Credentials::from_env().unwrap();
    let oauth = OAuth::from_env(scopes!("user-read-playback-state", "user-read-currently-playing")).unwrap();
    let mut spotify = AuthCodeSpotify::with_config(creds.clone(), oauth.clone(), Config {
        token_cached: true,
        token_refreshing: true,
        ..Default::default()
    });
    let url = spotify.get_authorize_url(false)?;
    spotify.prompt_for_token(&url).await?;

    let handler = SpotifyHandler::new(spotify);
    
    let cors = warp::cors()
        .allow_any_origin()
        .build();

    let playing_route = warp::path("playing")
        .and_then({
            let handler = handler.clone();
            move || get_status(handler.clone())
        }).with(&cors);

    let port = std::env::var("PORT").unwrap_or_else(|_| "3000".to_owned()).parse().expect("invalid port number");
    warp::serve(playing_route).run(([127, 0, 0, 1], port)).await;
    Ok(())
}
